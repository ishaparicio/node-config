#!/bin/bash
if [ "$(dpkg --print-architecture)" != "amd64" ]; then
  echo "Please switch to an AMD64-architecture instance"
  exit
fi
# Setup bash profile
echo "export PATH=/usr/local/go/bin:$PATH" >> $HOME/.bashrc
echo "export GOPATH=$HOME/go" >> $HOME/.bashrc
echo "rm ~/.bash_history &> /dev/null" >> $HOME/.bashrc
echo "rm ~/.sudo_as_admin_successful &> /dev/null" >> $HOME/.bashrc
echo "touch ~/.sudo_as_admin_successful" >> $HOME/.bashrc
echo "rm ~/.wget-hsts &> /dev/null" >> $HOME/.bashrc
# Update apt packages
sudo apt -y update
# Install git, gcc, g++, curl and jq
sudo apt -y install git gcc g++ curl jq
# Install node
sudo snap install node --channel=10/stable --classic
# Install yarn
npm install --global yarn
# Install go for amd64
sudo tar -C /usr/local -xzf $(pwd)/go1.15.14.linux-amd64.tar.gz
read -p "Press ENTER to continue and reboot"
sudo reboot
