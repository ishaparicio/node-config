# Songbird Node Config 

This repo provides complete instructions on how to setup your own full-history Songbird archival node on Digital Ocean.

## 🐤 Getting started

1. Make an account on Digital Ocean at https://www.digitalocean.com/.

2. In the top-left panel, navigate to '+ New Project' and fill in details for the project and then press skip on the 'Move Resources' prompt, for example:

![](img/node-config-1.png)

3. Navigate to your newly created Songbird project and then select 'Get Started with a Droplet'.

4. Select the following settings for your new instance's operating system and compute resources: 'Ubuntu 20.04 LTS x64' and 'Basic Shared-CPU plan' with 'Premium AMD with NVMe SSD' at 8GB / 4 AMD CPUs ($48/month).

![](img/node-config-2.png)

5. Then, select 'Add Block Storage' and pick 100GB (+$10/month), leaving the default settings in place of 'Automatically Format & Mount' and 'Ext4' filesystem. Select 'Frankfurt' for the data-centre region, or any other location that you prefer. 

![](img/node-config-3.png)

6. Next, leave VPC Network settings as the default value of 'default-fra1' - this default will be different if you selected a different data-centre region from Frankfurt. In 'Select additional options', select 'Monitoring' to be enabled. Finally, choose 'SSH keys' for Authentication and follow the instructions for generating a new SSH keypair. 

![](img/node-config-4.png)

7. Next, choose a hostname of 'songbird-frankfurt-1', or a different name depending on your data-centre region, and select 'Enable Backups' (+$9.60/month).

![](img/node-config-5.png)

8. Finally, select 'Create Droplet' and your server will begin initializing!

9. It will take a few moments for your instance to complete initializing, and once it's ready there will be a green indicator next to its name.

![](img/node-config-6.png)

10. Once it's ready, navigate to your instance by clicking its name, and you should see an instance dashboard with metrics like this:

![](img/node-config-7.png)

11. In the top-right corner of the instance dashboard page, select 'Console'. This will create an SSH session where you can directly command your instance. Once logged-in to your node, you can follow the instructions in the next section for installing Songbird.

![](img/node-config-8.png)

## 🦜 Installing dependencies

1. To begin, run the following command to download this repo to your instance:

```
git clone https://gitlab.com/flarenetwork/node-config.git
```

2. Next, navigate to the node-config folder and then install the songbird config using the following command. Note: always answer yes to any prompt in the install process.

```
cd node-config
./config.sh
```

This will install all dependencies and the Flare codebase, and then it will compile the Flare codebase for Songbird.

## 🦉 Registering Your IP Address For Peering

1. The next step during the 'Observation Period' of the Songbird network is to register your machine's IP address with the following Google form https://forms.gle/zHisUgitnSEHCGBb6. You can find and copy your machine's public IPv4 address in the top left-hand corner of the instance dashboard described in part 10 in the 'Getting Started' section above. Note that this requirement will eventually be removed from the network, but is just being run in the 'Observation Period', where there may be rollbacks or faults due to new code being deployed.

You should then receive a response within at most 24 hours indicating that your machine is green-lighted for peering to the network.

## 🦅 Launching Your Songbird Node

1. In your instance dashboard page, select 'Console' again to log back in to your server.

2. Next, run the following commands to start your Songbird node:

```
git clone https://gitlab.com/flarenetwork/flare.git
cd flare
./compile.sh songbird
./cmd/songbird.sh
```

When you see the final printout of "Songbird node successfully launched on PID: <PID>", you've completed the launch of your Songbird node!

3. You can then check the health status of your node using the command:

```
curl http://127.0.0.1:9650/ext/health | jq .
```

It may take some time for your node to complete bootstrapping to the network, and it will reflect `health: false` until it completes. You can also directly inspect the logs of your server in the `$HOME/flare/logs/` folder.

4. At any time, including before bootstrapping has finished, you can exit the console session without taking down the launched node using the command:

```
exit
```

# 🥳
